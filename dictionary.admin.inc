<?php

/**
 * @file
 * Administrative page callbacks for the dictionary module.
 */

/**
 * Given a language, list all items and their translations in that language.
 * Links to pages to modify and add translations.
 */
function dictionary_admin_language_translations($language) {
  $items = dictionary_get_items();
  $rows = array();
  foreach ($items as $item) {
    $result = db_query('SELECT * FROM {dictionary_translation} WHERE lid = %d AND iid = %d', $language->lid, $item->iid);
    $count = 0;
    while ($translation = db_fetch_object($result)) {
      $count++;
      $rows[] = array(
        'item' => l($item->name, "admin/content/dictionary/item/$item->iid"),
        'translation' => check_plain($translation->name),
        'edit' => l(t('edit'), "admin/content/dictionary/edit_translation/language/$translation->tid"),
        'add' => l(t('add'), "admin/content/dictionary/add_translation/language/$item->iid/$language->lid")
      );        
    }
    if ($count == 0) {
      $rows[] = array(
        'item' => l($item->name, "admin/content/dictionary/item/$item->iid"),
        'translation' => t('n/a'),
        'edit' => '',
        'add' => l(t('add'), "admin/content/dictionary/add_translation/language/$item->iid/$language->lid")
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '4'));
  }
  $header = array(t('Item'), t('Translation'), array('data' => t('Operations'), 'colspan' => '2'));

  return theme('table', $header, $rows, array('id' => 'dictionary'));
}

/**
 * Given an item, list all translations for that item.
 * Links to pages to modify and add translations.
 */
function dictionary_admin_item_translations($item) {
  $languages = dictionary_get_languages();
  $rows = array();
  foreach ($languages as $language) {
    $result = db_query('SELECT * FROM {dictionary_translation} WHERE lid = %d AND iid = %d', $language->lid, $item->iid);
    $count = 0;
    while ($translation = db_fetch_object($result)) {
      $count++;
      $rows[] = array(
        'item' => l($language->name, "admin/content/dictionary/language/$language->lid"),
        'translation' => check_plain($translation->name),
        'edit' => l(t('edit'), "admin/content/dictionary/edit_translation/item/$translation->tid"),
        'add' => l(t('add'), "admin/content/dictionary/add_translation/item/$item->iid/$language->lid")
      );
    }
    if ($count == 0) {
      $rows[] = array(
        'item' => l($language->name, "admin/content/dictionary/language/$language->lid"),
        'translation' => t('n/a'),
        'edit' => '',
        'add' => l(t('add'), "admin/content/dictionary/add_translation/item/$item->iid/$language->lid")
      );
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No languages available.'), 'colspan' => '4'));
  }
  $header = array(t('Language'), t('Translation'), array('data' => t('Operations'), 'colspan' => '2'));

  return theme('table', $header, $rows, array('id' => 'dictionary'));
}

/**
 * Form builder to list and manage items.
 *
 * @ingroup forms
 * @see theme_dictionary_overview_items()
 */
function dictionary_overview_items() {
  $items = dictionary_get_items();
  $data = array();
  foreach ($items as $item) {
    $data[$item->iid]['#item'] = (array)$item;
    $data[$item->iid]['name'] = array('#value' => check_plain($item->name));
    $data[$item->iid]['edit'] = array('#value' => l(t('edit item'), "admin/content/dictionary/edit_item/$item->iid"));
    $data[$item->iid]['manage'] = array('#value' => l(t('manage translations'), "admin/content/dictionary/item/$item->iid"));
  }

  return theme_dictionary_overview_items($data);
}

/**
 * Theme the item overview.
 *
 * @ingroup themeable
 * @see dictionary_overview_items()
 */
function theme_dictionary_overview_items($data) {
  $rows = array();
  foreach (element_children($data) as $key) {
    $item = &$data[$key];

    $row = array();
    $row[] = drupal_render($item['name']);
    $row[] = drupal_render($item['edit']);
    $row[] = drupal_render($item['manage']);
    $rows[] = array('data' => $row);   
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No items available.'), 'colspan' => '3'));
  }
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => '2'));

  return theme('table', $header, $rows, array('id' => 'dictionary'));
}

/**
 * Display form for adding and editing items.
 *
 * @ingroup forms
 * @see dictionary_form_item_submit()
 */
function dictionary_form_item(&$form_state, $edit = array()) {
  $edit += array(
    'name' => '',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Item name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('The name for this item.'),
    '#required' => TRUE,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  if (isset($edit['iid'])) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['iid'] = array('#type' => 'value', '#value' => $edit['iid']);
  }
  return $form;
}

/**
 * Accept the form submission for an item and save the results.
 */
function dictionary_form_item_submit($form, &$form_state) {
  switch (dictionary_save_item($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new item %name.', array('%name' => $form_state['values']['name'])));
      watchdog('dictionary', 'Created new item %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_item/'. $form_state['values']['iid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated item %name.', array('%name' => $form_state['values']['name'])));
      watchdog('dictionary', 'Updated item %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_item/'. $form_state['values']['iid']));
      $form_state['redirect'] = 'admin/content/dictionary/list_item';
      break;
  }

  $form_state['iid'] = $form_state['values']['iid'];
  return;
}

/**
 * Page to edit an item.
 */
function dictionary_admin_item_edit($item) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('dictionary_item_confirm_delete', $item->iid);
  }
  return drupal_get_form('dictionary_form_item', (array)$item);
}

/**
 * Display form for editing the general settings.
 *
 * @ingroup forms
 * @see dictionary_settings_form_submit()
 */
function dictionary_form_settings(&$form_state) {
  $form['dictionary_head'] = array(
    '#type' => 'textarea',
    '#title' => t('Dictionary introductory text'),
    '#default_value' => variable_get('dictionary_head', ''),
    '#description' => t('Message shown on the dictionary page above all other content.')
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

function dictionary_form_settings_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    variable_set($key, $value);
  }

  cache_clear_all();
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Form builder to list and manage languages.
 *
 * @ingroup forms
 * @see theme_dictionary_overview_languages()
 */
function dictionary_overview_languages() {
  $languages = dictionary_get_languages();
  $data = array();
  foreach ($languages as $language) {
    $data[$language->lid]['#language'] = (array)$language;
    $data[$language->lid]['name'] = array('#value' => check_plain($language->name));
    $data[$language->lid]['edit'] = array('#value' => l(t('edit language'), "admin/content/dictionary/edit_language/$language->lid"));
    $data[$language->lid]['manage'] = array('#value' => l(t('manage translations'), "admin/content/dictionary/language/$language->lid"));
  }

  return theme_dictionary_overview_languages($data);
}

/**
 * Theme the language overview.
 *
 * @ingroup themeable
 * @see dictionary_overview_languages()
 */
function theme_dictionary_overview_languages($data) {
  $rows = array();
  foreach (element_children($data) as $key) {
    $language = &$data[$key];

    $row = array();
    $row[] = drupal_render($language['name']);
    $row[] = drupal_render($language['edit']);
    $row[] = drupal_render($language['manage']);
    $rows[] = array('data' => $row);   
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No languages available.'), 'colspan' => '3'));
  }
  $header = array(t('Name'), array('data' => t('Operations'), 'colspan' => '2'));

  return theme('table', $header, $rows, array('id' => 'dictionary'));
}

/**
 * Display form for adding and editing languages.
 *
 * @ingroup forms
 * @see dictionary_form_language_submit()
 */
function dictionary_form_language(&$form_state, $edit = array()) {
  $edit += array(
    'name' => '',
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Language name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('The name for this language, e.g., <em>"German (Deutsch)"</em>.'),
    '#required' => TRUE,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  if (isset($edit['lid'])) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['lid'] = array('#type' => 'value', '#value' => $edit['lid']);
  }
  return $form;
}

/**
 * Accept the form submission for a language and save the results.
 */
function dictionary_form_language_submit($form, &$form_state) {
  switch (dictionary_save_language($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new language %name.', array('%name' => $form_state['values']['name'])));
      watchdog('dictionary', 'Created new language %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_language/'. $form_state['values']['lid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated language %name.', array('%name' => $form_state['values']['name'])));
      watchdog('dictionary', 'Updated language %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_language/'. $form_state['values']['lid']));
      $form_state['redirect'] = 'admin/content/dictionary/list_language';
      break;
  }

  $form_state['lid'] = $form_state['values']['lid'];
  return;
}

/**
 * Page to edit a language.
 */
function dictionary_admin_language_edit($language) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('dictionary_language_confirm_delete', $language->lid);
  }
  return drupal_get_form('dictionary_form_language', (array)$language);
}

/**
 * Page to edit a translation.
 */
function dictionary_admin_translation_edit($translation, $redirect) {
  if ((isset($_POST['op']) && $_POST['op'] == t('Delete')) || isset($_POST['confirm'])) {
    return drupal_get_form('dictionary_translation_confirm_delete', $translation->tid, $redirect);
  }
  return drupal_get_form('dictionary_form_translation', dictionary_item_load($translation->iid), dictionary_language_load($translation->lid), $redirect, (array)$translation);
}

/**
 * Form function for the translation edit form.
 *
 * @ingroup forms
 * @see dictionary_form_translation_submit()
 */
function dictionary_form_translation(&$form_state, $item, $language, $redirect, $edit = array()) {
  $edit += array(
    'name' => '',
  );

  $form['#translation'] = $edit;
  $form['#item'] = (array)$item;
  $form['#language'] = (array)$language;
  if ($redirect == 'language') {
    $form['#redirect'] = 'admin/content/dictionary/language/'. $language->lid;
  }
  else {
    $form['#redirect'] = 'admin/content/dictionary/item/'. $item->iid;
  }

  // Check for confirmation forms.
  if (isset($form_state['confirm_delete'])) {
    return array_merge($form, dictionary_translation_confirm_delete($form_state, $edit['tid']));
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Translation name'),
    '#default_value' => $edit['name'],
    '#maxlength' => 255,
    '#description' => t('The name of this translation.'),
    '#required' => TRUE);

  $form['iid'] = _dictionary_item_select(t('Item'), 'iid', $item->iid);
  $form['lid'] =  _dictionary_language_select(t('Language'), 'lid', $language->lid);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'));

  if ($edit['tid']) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'));
    $form['tid'] = array(
      '#type' => 'value',
      '#value' => $edit['tid']);
  }

  return $form;
}

/**
 * Submit handler to insert or update a translation.
 *
 * @see dictionary_form_translation()
 */
function dictionary_form_translation_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Delete')) {
    // Execute the translation deletion.
    if ($form_state['values']['delete'] === TRUE) {
      return dictionary_translation_confirm_delete_submit($form, $form_state);
    }
    // Rebuild the form to confirm translation deletion.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_delete'] = TRUE;
    return;
  }

  switch (dictionary_save_translation($form_state['values'])) {
    case SAVED_NEW:
      drupal_set_message(t('Created new translation %translation.', array('%translation' => $form_state['values']['name'])));
      watchdog('dictionary', 'Created new translation %translation.', array('%translation' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_translation/item/'. $form_state['values']['tid']));
      break;
    case SAVED_UPDATED:
      drupal_set_message(t('Updated translation %translation.', array('%translation' => $form_state['values']['name'])));
      watchdog('dictionary', 'Updated translation %translation.', array('%translation' => $form_state['values']['name']), WATCHDOG_NOTICE, l(t('edit'), 'admin/content/dictionary/edit_translation/item/'. $form_state['values']['tid']));
      break;
  }

  $form_state['tid'] = $form_state['values']['tid'];
  return;
}

/**
 * Form builder for the translation delete form.
 *
 * @ingroup forms
 * @see dictionary_translation_confirm_delete_submit()
 */
function dictionary_translation_confirm_delete(&$form_state, $tid, $redirect) {
  $translation = dictionary_translation_load($tid);

  $form['type'] = array('#type' => 'value', '#value' => 'translation');
  $form['name'] = array('#type' => 'value', '#value' => $translation->name);
  $form['tid'] = array('#type' => 'value', '#value' => $tid);
  $form['delete'] = array('#type' => 'value', '#value' => TRUE);
  if ($redirect == 'language') {
    $form['#redirect'] = 'admin/content/dictionary/language/'. $translation->lid;
  }
  else {
    $form['#redirect'] = 'admin/content/dictionary/item/'. $translation->iid;
  }

  return confirm_form($form,
                  t('Are you sure you want to delete the translation %title?',
                  array('%title' => $translation->name)),
                  'admin/content/dictionary',
                  t('This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a translation after confirmation.
 *
 * @see dictionary_translation_confirm_delete()
 */
function dictionary_translation_confirm_delete_submit($form, &$form_state) {
  dictionary_del_translation($form_state['values']['tid']);
  drupal_set_message(t('Deleted translation %name.', array('%name' => $form_state['values']['name'])));
  watchdog('dictionary', 'Deleted translation %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  return;
}

/**
 * Form builder for the language delete confirmation form.
 *
 * @ingroup forms
 * @see dictionary_language_confirm_delete_submit()
 */
function dictionary_language_confirm_delete(&$form_state, $lid) {
  $language = dictionary_language_load($lid);

  $form['type'] = array('#type' => 'value', '#value' => 'language');
  $form['lid'] = array('#type' => 'value', '#value' => $lid);
  $form['name'] = array('#type' => 'value', '#value' => $language->name);
  return confirm_form($form,
                  t('Are you sure you want to delete the language %title?',
                  array('%title' => $language->name)),
                  'admin/content/dictionary',
                  t('Deleting a language will delete all the translations in it. This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete a language after confirmation.
 *
 * @see dictionary_language_confirm_delete()
 */
function dictionary_language_confirm_delete_submit($form, &$form_state) {
  $status = dictionary_del_language($form_state['values']['lid']);
  drupal_set_message(t('Deleted language %name.', array('%name' => $form_state['values']['name'])));
  watchdog('dictionary', 'Deleted language %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/content/dictionary';
  return;
}

/**
 * Form builder for the item delete confirmation form.
 *
 * @ingroup forms
 * @see dictionary_item_confirm_delete_submit()
 */
function dictionary_item_confirm_delete(&$form_state, $iid) {
  $item = dictionary_item_load($iid);

  $form['type'] = array('#type' => 'value', '#value' => 'item');
  $form['iid'] = array('#type' => 'value', '#value' => $iid);
  $form['name'] = array('#type' => 'value', '#value' => $item->name);
  return confirm_form($form,
                  t('Are you sure you want to delete the item %title?',
                  array('%title' => $item->name)),
                  'admin/content/dictionary',
                  t('Deleting an item will delete all the translations for it. This action cannot be undone.'),
                  t('Delete'),
                  t('Cancel'));
}

/**
 * Submit handler to delete an item after confirmation.
 *
 * @see dictionary_item_confirm_delete()
 */
function dictionary_item_confirm_delete_submit($form, &$form_state) {
  $status = dictionary_del_item($form_state['values']['iid']);
  drupal_set_message(t('Deleted item %name.', array('%name' => $form_state['values']['name'])));
  watchdog('dictionary', 'Deleted item %name.', array('%name' => $form_state['values']['name']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/content/dictionary';
  return;
}
