<?php

/**
 * Available variables:
 * $dictionary['from']: The language object for the source language.
 * $dictionary['to']: The language object for the target language.
 * $dictionary['translations']: An array of translations. Each row has a 'source' translation object and an array 'targets' of translation objects. If a node exists that is associated with the item, then 'nid' is set accordingly.
 *
 * Example use cases:
 * print t('Translations from %source_lang to %target_lang', array('%source_lang' => $dictionary['from']->name, '%target_lang' => $dictionary['to']->name));
 * print '<dl>';
 * foreach ($dictionary['translations'] as $translation) {
 *   if (!$translation['nid']) {
 *     print '<dt>'. $translation['source']->name .'</dt>';
 *   }
 *   else {
 *     print '<dt>'. l($translation['source']->name, 'node/'. $translation['nid']) .'</dt>';
 *   }
 *   print '<dd><ul>';
 *   foreach ($translation['targets'] as $target) {
 *     print '<li>'. $target->name .'</li>';
 *   }
 *   print '</ul></dd>';
 * }
 * print '</dl>';
 */
?>

<div id="dictionary-page">
  <div id="dictionary-head"><?php print $dictionary['head']; ?></div>

  <div id="dictionary-search">
    <h3><?php print t('Search for terms'); ?></h3>
    <?php
      $search_form = drupal_get_form('search_form', NULL, NULL, 'dictionary');
      // Remove label from the search form.
      preg_replace('/<label(.*)>(.*)<\/label>\n/i', '', $search_form);
      print $search_form;
    ?>
  </div>

  <h3><?php print t('Translate all terms from one language to another'); ?></h3>
  <?php
    print drupal_get_form('dictionary_form_from_to', $dictionary['from']->lid, $dictionary['to']->lid);
    print theme('dictionary_translations', $dictionary['from'], $dictionary['to'], $dictionary['translations'], '-');	
  ?>
</div>
