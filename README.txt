$Id

Dictionary
------------------------

To install, place the entire dictionary folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Dictionary module.

First go to Administer -> User management -> Permissions and set administer and
access permissions as required.

Now go to Administer -> Content management -> Dictionary. Create at least one
language and at least one item. Then test by adding a translation for the items
into the languages.

Next go to Create content -> Dictionary term. Create a node for one of your
dictionary items.

Finally go to Administer -> Menu -> Navigation and enable the Dictionary menu item.
Now visit the dictionary page.

You will see a search box, in which you can search for terms. Note that you can only
find translations of terms that have an associated 'Dictionary term' page.
Next comes a form where you can specify two languages. After submitting all terms in
one language will be translated to the other. For all items where a Dictionary term
page was created, the term in the source language (on the left) links to that page.

If you want all translations for an item to be listed on 'Dictionary term' pages, then
copy the node-dictionary_term.tpl.php file to your theme directory. You can then also
customize how these pages are displayed. The dictionary page itself can be customised by
copying the dictionary.tpl.php to your theme directory and modifying it.


Maintainers
-----------
The Dictionary was developed by:
Sara Adams (http://www.sara-adams.de)

The Dictionary is currently being maintained by:
Sara Adams